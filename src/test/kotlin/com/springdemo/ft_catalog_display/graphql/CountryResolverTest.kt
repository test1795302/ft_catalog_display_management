package com.springdemo.ft_catalog_display.graphql

import com.springdemo.ft_catalog_display.dto.CountryDto
import com.springdemo.ft_catalog_display.service.CountryService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class CountryResolverTest {

    @Mock
    private lateinit var countryService: CountryService

    @InjectMocks
    private lateinit var countryResolver: CountryResolver

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testGetAllCountries() {
        val country1 = CountryDto(1L, "Country1")
        val country2 = CountryDto(2L, "Country2")
        val expectedCountries = listOf(country1, country2)

        `when`(countryService.getAllCountries()).thenReturn(expectedCountries)

        val result = countryResolver.getAllCountries()

        assertEquals(expectedCountries, result)
    }
}
