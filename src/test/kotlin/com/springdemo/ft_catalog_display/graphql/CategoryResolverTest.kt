package com.springdemo.ft_catalog_display.graphql

import com.springdemo.ft_catalog_display.dto.CategoryDto
import com.springdemo.ft_catalog_display.service.CategoryService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class CategoryResolverTest {

    @Mock
    private lateinit var categoryService: CategoryService

    @InjectMocks
    private lateinit var categoryResolver: CategoryResolver

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testGetAllCategories() {
        val category1 = CategoryDto(1L, "Category1")
        val category2 = CategoryDto(2L, "Category2")
        val expectedCategories = listOf(category1, category2)

        `when`(categoryService.getCategories()).thenReturn(expectedCategories)

        val result = categoryResolver.getAllCategories()

        assertEquals(expectedCategories, result)
    }
}
