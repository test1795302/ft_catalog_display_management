package com.springdemo.ft_catalog_display.graphql

import com.springdemo.ft_catalog_display.dto.FeatureDto
import com.springdemo.ft_catalog_display.service.FeatureService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class FeatureResolverTest {

    @Mock
    private lateinit var featureService: FeatureService

    @InjectMocks
    private lateinit var featureResolver: FeatureResolver

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testGetAllFeatures() {
        val feature1 = FeatureDto(1L, "Feature1")
        val feature2 = FeatureDto(2L, "Feature2")
        val expectedFeatures = listOf(feature1, feature2)

        `when`(featureService.getAllFeatures()).thenReturn(expectedFeatures)

        val result = featureResolver.getAllFeatures()

        assertEquals(expectedFeatures, result)
    }
}
