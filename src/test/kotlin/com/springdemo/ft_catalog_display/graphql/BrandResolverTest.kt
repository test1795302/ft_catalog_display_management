package com.springdemo.ft_catalog_display.graphql

import com.springdemo.ft_catalog_display.dto.BrandDto
import com.springdemo.ft_catalog_display.service.BrandService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class BrandResolverTest {

    @Mock
    private lateinit var brandService: BrandService

    @InjectMocks
    private lateinit var brandResolver: BrandResolver

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testGetAllBrands() {
        val brand1 = BrandDto(1L, "Brand1")
        val brand2 = BrandDto(2L, "Brand2")
        val expectedBrands = listOf(brand1, brand2)

        `when`(brandService.getBrands()).thenReturn(expectedBrands)

        val result = brandResolver.getAllBrands()

        assertEquals(expectedBrands, result)
    }
}
