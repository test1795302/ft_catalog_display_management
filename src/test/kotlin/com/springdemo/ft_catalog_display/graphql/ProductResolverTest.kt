package com.springdemo.ft_catalog_display.graphql

import com.springdemo.ft_catalog_display.dto.BrandDto
import com.springdemo.ft_catalog_display.dto.CategoryDto
import com.springdemo.ft_catalog_display.dto.CountryDto
import com.springdemo.ft_catalog_display.dto.ProductDto
import com.springdemo.ft_catalog_display.service.ProductService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class ProductResolverTest {

    @Mock
    private lateinit var productService: ProductService

    @InjectMocks
    private lateinit var productResolver: ProductResolver

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testGetProductById() {
        val productId = 1L
        val expectedProduct = ProductDto(productId, "Product1", "Description1", 10.0, "Logo1", CountryDto(1L, "CountryName1"),
                CategoryDto(2L, "CategoryName2"),
                BrandDto(3L, "BrandName3")
        )

        `when`(productService.getProductById(productId)).thenReturn(expectedProduct)
        val result = productResolver.getProductById(productId)

        assertEquals(expectedProduct, result)
    }

    @Test
    fun testGetAllProducts() {
        val product1 = ProductDto(1L, "Product1", "Description1", 10.0, "Logo1", CountryDto(1L, "CountryName1"),
                CategoryDto(2L, "CategoryName2"),
                BrandDto(3L, "BrandName3"))
        val product2 = ProductDto(2L, "Product2", "Description2", 20.0, "Logo2", CountryDto(4L, "CountryName4"),
            CategoryDto(5L, "CategoryName6"),
            BrandDto(6L, "BrandName6"))
        val expectedProducts = listOf(product1, product2)

        `when`(productService.getAllProducts()).thenReturn(expectedProducts)

        val result = productResolver.getAllProducts()
        assertEquals(expectedProducts, result)
    }

    @Test
    fun testFilterProducts() {
        val filteredProduct1 = ProductDto(1L, "Product1", "Description1", 10.0, "Logo1", CountryDto(1L, "CountryName1"),
            CategoryDto(2L, "CategoryName2"),
            BrandDto(3L, "BrandName3"))
        val filteredProduct2 = ProductDto(2L, "Product2", "Description2", 20.0, "Logo2", CountryDto(4L, "CountryName4"),
            CategoryDto(5L, "CategoryName6"),
            BrandDto(6L, "BrandName6"))
        val expectedFilteredProducts = listOf(filteredProduct1, filteredProduct2)

        `when`(productService.filterProducts(null, null, null, null,null,null, 1L, 2L)).thenReturn(expectedFilteredProducts)

        val result = productResolver.filterProducts(null, null, null, null,null,null, 1L, 2L)

        assertEquals(expectedFilteredProducts, result)
    }
}
