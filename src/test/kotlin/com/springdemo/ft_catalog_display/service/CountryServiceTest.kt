package com.springdemo.ft_catalog_display.service

import com.springdemo.ft_catalog_display.dto.CategoryDto
import com.springdemo.ft_catalog_display.dto.CountryDto
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.CategoryRecord
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.CountryRecord
import com.springdemo.ft_catalog_display.repository.CountryRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class CountryServiceTest {

    @Mock
    private lateinit var countryRepository: CountryRepository

    @InjectMocks
    private lateinit var countryService: CountryService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testGetAllCountries() {
        val countryRecord1 = CountryRecord(1L, "Country1")
        val countryRecord2 = CountryRecord(2L, "Country2")

        `when`(countryRepository.getAllCountries()).thenReturn(listOf(countryRecord1, countryRecord2))

        val result = countryService.getAllCountries()

        assertEquals(2, result.size)
        assertEquals(CountryDto(1L, "Country1"), result[0])
        assertEquals(CountryDto(2L, "Country2"), result[1])
    }

    @Test
    fun testGetCountryById() {
        val countryId = 1L
        val countryRecord = CountryRecord(countryId, "TestCountry")

        `when`(countryRepository.getCountryById(countryId)).thenReturn(countryRecord)

        val result = countryService.getCountryById(countryId)

        assertEquals(CountryDto(countryId, "TestCountry"), result)
    }

}
