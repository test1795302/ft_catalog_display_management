package com.springdemo.ft_catalog_display.service

import com.springdemo.ft_catalog_display.dto.FeatureDto
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.FeatureRecord
import com.springdemo.ft_catalog_display.repository.FeatureRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class FeatureServiceTest {

    @Mock
    private lateinit var featureRepository: FeatureRepository

    @InjectMocks
    private lateinit var featureService: FeatureService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testGetAllFeatures() {

        val featureRecord1 = FeatureRecord(1L, "Feature1")
        val featureRecord2 = FeatureRecord(2L, "Feature2")

        `when`(featureRepository.getAllFeatures()).thenReturn(listOf(featureRecord1, featureRecord2))

        val result = featureService.getAllFeatures()

        assertEquals(2, result.size)
        assertEquals(FeatureDto(1L, "Feature1"), result[0])
        assertEquals(FeatureDto(2L, "Feature2"), result[1])
    }

}
