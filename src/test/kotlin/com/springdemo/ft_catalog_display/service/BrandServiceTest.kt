package com.springdemo.ft_catalog_display.service

import com.springdemo.ft_catalog_display.dto.BrandDto
import com.springdemo.ft_catalog_display.dto.CountryDto
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.BrandRecord
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.CountryRecord
import com.springdemo.ft_catalog_display.repository.BrandRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class BrandServiceTest {

    @Mock
    private lateinit var brandRepository: BrandRepository

    @InjectMocks
    private lateinit var brandService: BrandService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testGetBrands() {
        val brandRecord1 = BrandRecord(1L, "Brand1")
        val brandRecord2 = BrandRecord(2L, "Brand2")

        `when`(brandRepository.getBrands()).thenReturn(listOf(brandRecord1, brandRecord2))

        val result = brandService.getBrands()

        assertEquals(2, result.size)
        assertEquals(BrandDto(1L, "Brand1"), result[0])
        assertEquals(BrandDto(2L, "Brand2"), result[1])
    }

    @Test
    fun testGetBrandsById() {
        val brandId = 1L
        val brandRecord = BrandRecord(brandId, "TestBrand")

        `when`(brandRepository.getBrandById(brandId)).thenReturn(brandRecord)

        val result = brandService.getBrandsById(brandId)

        assertEquals(BrandDto(brandId, "TestBrand"), result)
    }
}
