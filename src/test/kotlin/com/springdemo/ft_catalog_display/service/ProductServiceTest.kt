package com.springdemo.ft_catalog_display.service

import com.springdemo.ft_catalog_display.dto.BrandDto
import com.springdemo.ft_catalog_display.dto.CategoryDto
import com.springdemo.ft_catalog_display.dto.CountryDto
import com.springdemo.ft_catalog_display.dto.ProductDto
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.ProductRecord
import com.springdemo.ft_catalog_display.repository.ProductRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class ProductServiceTest {

    @Mock
    private lateinit var productRepository: ProductRepository

    @Mock
    private lateinit var countryService: CountryService

    @Mock
    private lateinit var categoryService: CategoryService

    @Mock
    private lateinit var brandService: BrandService

    @InjectMocks
    private lateinit var productService: ProductService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testGetProductById() {
        val productId = 1L
        val productRecord = ProductRecord().apply {
            this.productId = productId
            this.title = "Product1"
            this.description = "Description1"
            this.price = 10.0
            this.logo = "Logo1"
            this.countryId = 2L
            this.brandId = 3L
            this.categoryId = 4L
        }

        `when`(productRepository.getProductById(productId)).thenReturn(productRecord)
        `when`(countryService.getCountryById(2L)).thenReturn(CountryDto(2L, "CountryName"))
        `when`(categoryService.getCategory(4L)).thenReturn(CategoryDto(4L, "CategoryName"))
        `when`(brandService.getBrandsById(3L)).thenReturn(BrandDto(3L, "BrandName"))

        val result = productService.getProductById(productId)

        assertEquals(
            ProductDto(
                productId,
                "Product1",
                "Description1",
                10.0,
                "Logo1",
                CountryDto(2L, "CountryName"),
                CategoryDto(4L, "CategoryName"),
                BrandDto(3L, "BrandName")
            ),
            result
        )
    }

    @Test
    fun testGetAllProducts() {
        val productRecord1 = ProductRecord().apply {
            this.productId = 1L
            this.title = "Product1"
            this.description = "Description1"
            this.price = 10.0
            this.logo = "Logo1"
            this.countryId = 2L
            this.brandId = 3L
            this.categoryId = 4L
        }
        val productRecord2 = ProductRecord().apply {
            this.productId = 2L
            this.title = "Product2"
            this.description = "Description2"
            this.price = 20.0
            this.logo = "Logo2"
            this.countryId = 5L
            this.brandId = 6L
            this.categoryId = 7L
        }

        `when`(productRepository.getProducts()).thenReturn(listOf(productRecord1, productRecord2))
        `when`(countryService.getCountryById(2L)).thenReturn(CountryDto(2L, "CountryName"))
        `when`(countryService.getCountryById(5L)).thenReturn(CountryDto(5L, "AnotherCountryName"))
        `when`(categoryService.getCategory(4L)).thenReturn(CategoryDto(4L, "CategoryName"))
        `when`(categoryService.getCategory(7L)).thenReturn(CategoryDto(7L, "AnotherCategoryName"))
        `when`(brandService.getBrandsById(3L)).thenReturn(BrandDto(3L, "BrandName"))
        `when`(brandService.getBrandsById(6L)).thenReturn(BrandDto(6L, "AnotherBrandName"))

        val result = productService.getAllProducts()

        assertEquals(2, result.size)
        assertEquals(
            ProductDto(
                1L,
                "Product1",
                "Description1",
                10.0,
                "Logo1",
                CountryDto(2L, "CountryName"),
                CategoryDto(4L, "CategoryName"),
                BrandDto(3L, "BrandName")
            ),
            result[0]
        )
        assertEquals(
            ProductDto(
                2L,
                "Product2",
                "Description2",
                20.0,
                "Logo2",
                CountryDto(5L, "AnotherCountryName"),
                CategoryDto(7L, "AnotherCategoryName"),
                BrandDto(6L, "AnotherBrandName")
            ),
            result[1]
        )
    }

    @Test
    fun testFilterProducts() {
        val productRecord1 = ProductRecord().apply {
            this.productId = 1L
            this.title = "Product1"
            this.description = "Description1"
            this.price = 10.0
            this.logo = "Logo1"
            this.countryId = 2L
            this.brandId = 3L
            this.categoryId = 4L
        }
        val productRecord2 = ProductRecord().apply {
            this.productId = 2L
            this.title = "Product2"
            this.description = "Description2"
            this.price = 20.0
            this.logo = "Logo2"
            this.countryId = 5L
            this.brandId = 6L
            this.categoryId = 7L
        }

        `when`(productRepository.filterProducts(null, null, null, null, null, null, 1L, 2L))
            .thenReturn(listOf(productRecord1, productRecord2))
        `when`(countryService.getCountryById(2L)).thenReturn(CountryDto(2L, "CountryName"))
        `when`(countryService.getCountryById(5L)).thenReturn(CountryDto(5L, "AnotherCountryName"))
        `when`(categoryService.getCategory(4L)).thenReturn(CategoryDto(4L, "CategoryName"))
        `when`(categoryService.getCategory(7L)).thenReturn(CategoryDto(7L, "AnotherCategoryName"))
        `when`(brandService.getBrandsById(3L)).thenReturn(BrandDto(3L, "BrandName"))
        `when`(brandService.getBrandsById(6L)).thenReturn(BrandDto(6L, "AnotherBrandName"))

        val result = productService.filterProducts(null, null, null, null, null, null, 1L, 2L)

        assertEquals(2, result.size)
        assertEquals(
            ProductDto(
                1L,
                "Product1",
                "Description1",
                10.0,
                "Logo1",
                CountryDto(2L, "CountryName"),
                CategoryDto(4L, "CategoryName"),
                BrandDto(3L, "BrandName")
            ),
            result[0]
        )
        assertEquals(
            ProductDto(
                2L,
                "Product2",
                "Description2",
                20.0,
                "Logo2",
                CountryDto(5L, "AnotherCountryName"),
                CategoryDto(7L, "AnotherCategoryName"),
                BrandDto(6L, "AnotherBrandName")
            ),
            result[1]
        )
    }
}
