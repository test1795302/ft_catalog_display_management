package com.springdemo.ft_catalog_display.service

import com.springdemo.ft_catalog_display.dto.CategoryDto
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.CategoryRecord
import com.springdemo.ft_catalog_display.repository.CategoryRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class CategoryServiceTest {

    @Mock
    private lateinit var categoryRepository: CategoryRepository

    @InjectMocks
    private lateinit var categoryService: CategoryService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testGetCategories() {
        val categoryRecord1 = CategoryRecord(1L, "Category1")
        val categoryRecord2 = CategoryRecord(2L, "Category2")

        `when`(categoryRepository.getCategories()).thenReturn(listOf(categoryRecord1, categoryRecord2))

        val result = categoryService.getCategories()

        assertEquals(2, result.size)
        assertEquals(CategoryDto(1L, "Category1"), result[0])
        assertEquals(CategoryDto(2L, "Category2"), result[1])
    }

    @Test
    fun testGetCategory() {
        val categoryId = 1L
        val categoryRecord = CategoryRecord(categoryId, "TestCategory")

        `when`(categoryRepository.getCategoryById(categoryId)).thenReturn(categoryRecord)

        val result = categoryService.getCategory(categoryId)

        assertEquals(CategoryDto(categoryId, "TestCategory"), result)
    }

}
