package com.springdemo.ft_catalog_display.repository

import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.Tables
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.Tables.CATEGORY
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.CategoryRecord
import org.jooq.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class CategoryRepositoryTest {

    @Mock
    private lateinit var dslContext: DSLContext

    @InjectMocks
    private lateinit var categoryRepository: CategoryRepository

    @BeforeEach
    fun setUp() {
        val mockSelectStep: SelectSelectStep<Record> = mock(SelectSelectStep::class.java) as SelectSelectStep<Record>
        `when`(dslContext.select()).thenReturn(mockSelectStep)
    }

    @Test
    fun `test getCategories`() {
        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        `when`(mockSelectStep.fetchInto(CategoryRecord::class.java)).thenReturn(listOf<CategoryRecord>())
        `when`(dslContext.select().from(Tables.CATEGORY)).thenReturn(mockSelectStep)

        val result = categoryRepository.getCategories()
    }

    @Test
    fun `test getCategoryById`() {
        val categoryId = 1L

        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        val mockSelectConditionStep: SelectConditionStep<Record> = mock(SelectConditionStep::class.java) as SelectConditionStep<Record>
        val mockCategoryRecord = mock(CategoryRecord::class.java)

        `when`(mockSelectConditionStep.fetchOneInto(CategoryRecord::class.java)).thenReturn(mockCategoryRecord)
        `when`(dslContext.select().from(Tables.CATEGORY)).thenReturn(mockSelectStep)
        `when`(mockSelectStep.where(CATEGORY.CATEGORY_ID.eq(categoryId))).thenReturn(mockSelectConditionStep)

        val result = categoryRepository.getCategoryById(categoryId)

        assertEquals(mockCategoryRecord, result)
    }

    @Test
    fun `test getCategoryByName`() {
        val categoryName = "Test Category"

        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        val mockSelectConditionStep: SelectConditionStep<Record> = mock(SelectConditionStep::class.java) as SelectConditionStep<Record>
        val mockCategoryRecord = mock(CategoryRecord::class.java)

        `when`(mockSelectConditionStep.fetchOneInto(CategoryRecord::class.java)).thenReturn(mockCategoryRecord)
        `when`(dslContext.select().from(CATEGORY)).thenReturn(mockSelectStep)
        `when`(mockSelectStep.where(CATEGORY.CATEGORY_NAME.eq(categoryName))).thenReturn(mockSelectConditionStep)

        val result = categoryRepository.getCategoryByName(categoryName)

        assertEquals(mockCategoryRecord, result)
    }
}
