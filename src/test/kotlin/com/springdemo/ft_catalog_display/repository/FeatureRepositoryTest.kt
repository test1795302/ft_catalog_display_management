package com.springdemo.ft_catalog_display.repository

import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.Tables
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.Feature.FEATURE
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.FeatureRecord
import org.jooq.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class FeatureRepositoryTest {

    @Mock
    private lateinit var dslContext: DSLContext

    @InjectMocks
    private lateinit var featureRepository: FeatureRepository

    @BeforeEach
    fun setUp() {
        val mockSelectStep: SelectSelectStep<Record> = mock(SelectSelectStep::class.java) as SelectSelectStep<Record>
        `when`(dslContext.select()).thenReturn(mockSelectStep)
    }

    @Test
    fun `test getAllFeatures`() {
        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        `when`(mockSelectStep.fetchInto(FeatureRecord::class.java)).thenReturn(listOf<FeatureRecord>())
        `when`(dslContext.select().from(Tables.FEATURE)).thenReturn(mockSelectStep)

        val result = featureRepository.getAllFeatures()
    }

    @Test
    fun `test getFeatureByName`() {
        val featureName = "Test Feature"

        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        val mockSelectConditionStep: SelectConditionStep<Record> = mock(SelectConditionStep::class.java) as SelectConditionStep<Record>
        val mockFeatureRecord = mock(FeatureRecord::class.java)

        `when`(mockSelectConditionStep.fetchOneInto(FeatureRecord::class.java)).thenReturn(mockFeatureRecord)
        `when`(dslContext.select().from(Tables.FEATURE)).thenReturn(mockSelectStep)
        `when`(mockSelectStep.where(Tables.FEATURE.FEATURE_NAME.eq(featureName))).thenReturn(mockSelectConditionStep)

        val result = featureRepository.getFeatureByName(featureName)
        assertEquals(mockFeatureRecord, result)
    }


    @Test
    fun `test getFeatureById`() {
        val featureId = 1L
        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        val mockSelectConditionStep: SelectConditionStep<Record> = mock(SelectConditionStep::class.java) as SelectConditionStep<Record>
        val mockFeatureRecord = mock(FeatureRecord::class.java)

        `when`(mockSelectConditionStep.fetchOneInto(FeatureRecord::class.java)).thenReturn(mockFeatureRecord)
        `when`(dslContext.select().from(Tables.FEATURE)).thenReturn(mockSelectStep)
        `when`(mockSelectStep.where(Tables.FEATURE.FEATURE_ID.eq(featureId))).thenReturn(mockSelectConditionStep)

        val result = featureRepository.getFeatureById(featureId)
        assertEquals(mockFeatureRecord, result)
    }
}
