package com.springdemo.ft_catalog_display.repository

import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.Tables
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.Product.PRODUCT
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.ProductRecord
import org.jooq.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import org.jooq.DSLContext
import org.jooq.SelectJoinStep

@ExtendWith(MockitoExtension::class)
class ProductRepositoryTest {

    @Mock
    private lateinit var dslContext: DSLContext

    @InjectMocks
    private lateinit var productRepository: ProductRepository

    @BeforeEach
    fun setUp() {
        val mockSelectStep: SelectSelectStep<Record> = mock(SelectSelectStep::class.java) as SelectSelectStep<Record>
        `when`(dslContext.select()).thenReturn(mockSelectStep)
    }

    @Test
    fun `test getProducts`() {
        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        `when`(mockSelectStep.fetchInto(ProductRecord::class.java)).thenReturn(listOf<ProductRecord>())
        `when`(dslContext.select().from(Tables.PRODUCT)).thenReturn(mockSelectStep)

        val result = productRepository.getProducts()
    }

    @Test
    fun `test getProductById`() {
        val productId = 1L

        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        val mockSelectConditionStep: SelectConditionStep<Record> = mock(SelectConditionStep::class.java) as SelectConditionStep<Record>
        val mockProductRecord = mock(ProductRecord::class.java)

        `when`(mockSelectConditionStep.fetchOneInto(ProductRecord::class.java)).thenReturn(mockProductRecord)
        `when`(dslContext.select().from(Tables.PRODUCT)).thenReturn(mockSelectStep)
        `when`(mockSelectStep.where(PRODUCT.PRODUCT_ID.eq(productId))).thenReturn(mockSelectConditionStep)

        val result = productRepository.getProductById(productId)

        assertEquals(mockProductRecord, result)
    }

//    @Test
//    fun `test filterProducts`() {
//        val selectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
//        `when`(dslContext.select().from(Tables.PRODUCT)).thenReturn(selectStep)
//
//        val country = "USA"
//        val category = "Electronics"
//        val feature = listOf("Wireless", "Bluetooth")
//        val brand = listOf("Samsung", "Apple")
//        val search = "searchString"
//        val sort = "sortString"
//        val pageNumber = 1L
//        val limit = 10L
//
//        val productRecords = listOf<ProductRecord>()
//        `when`(selectStep.fetchInto(ProductRecord::class.java)).thenReturn(productRecords)
//
//        val result = productRepository.filterProducts(country, category, feature, brand, search, sort, pageNumber, limit)
//
//        assertEquals(productRecords, result)
//    }

}
