package com.springdemo.ft_catalog_display.repository

import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.Tables
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.Tables.COUNTRY
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.CountryRecord
import org.jooq.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class CountryRepositoryTest {

    @Mock
    private lateinit var dslContext: DSLContext

    @InjectMocks
    private lateinit var countryRepository: CountryRepository

    @BeforeEach
    fun setUp() {
        val mockSelectStep: SelectSelectStep<Record> = mock(SelectSelectStep::class.java) as SelectSelectStep<Record>
        `when`(dslContext.select()).thenReturn(mockSelectStep)
    }


    @Test
    fun `test getAllCountries`() {
        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        `when`(mockSelectStep.fetchInto(CountryRecord::class.java)).thenReturn(listOf<CountryRecord>())
        `when`(dslContext.select().from(Tables.COUNTRY)).thenReturn(mockSelectStep)

        val result = countryRepository.getAllCountries()
    }

    @Test
    fun `test getCountryById`() {
        val countryId = 1L

        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        val mockSelectConditionStep: SelectConditionStep<Record> = mock(SelectConditionStep::class.java) as SelectConditionStep<Record>
        val mockCountryRecord = mock(CountryRecord::class.java)

        `when`(mockSelectConditionStep.fetchOneInto(CountryRecord::class.java)).thenReturn(mockCountryRecord)
        `when`(dslContext.select().from(Tables.COUNTRY)).thenReturn(mockSelectStep)
        `when`(mockSelectStep.where(COUNTRY.COUNTRY_ID.eq(countryId))).thenReturn(mockSelectConditionStep)

        val result = countryRepository.getCountryById(countryId)

        assertEquals(mockCountryRecord, result)
    }

    @Test
    fun `test getCountryByName`() {
        val countryName = "Test Country"

        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        val mockSelectConditionStep: SelectConditionStep<Record> = mock(SelectConditionStep::class.java) as SelectConditionStep<Record>
        val mockCountryRecord = mock(CountryRecord::class.java)

        `when`(mockSelectConditionStep.fetchOneInto(CountryRecord::class.java)).thenReturn(mockCountryRecord)
        `when`(dslContext.select().from(Tables.COUNTRY)).thenReturn(mockSelectStep)
        `when`(mockSelectStep.where(COUNTRY.COUNTRY_NAME.eq(countryName))).thenReturn(mockSelectConditionStep)

        val result = countryRepository.getCountryByName(countryName)

        assertEquals(mockCountryRecord, result)
    }
}
