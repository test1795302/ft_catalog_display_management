package com.springdemo.ft_catalog_display.repository

import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.Tables.BRAND
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.BrandRecord
import org.jooq.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class BrandRepositoryTest {

    @Mock
    private lateinit var dslContext: DSLContext

    @InjectMocks
    private lateinit var brandRepository: BrandRepository

    @BeforeEach
    fun setUp() {
        val mockSelectStep: SelectSelectStep<Record> = mock(SelectSelectStep::class.java) as SelectSelectStep<Record>
        `when`(dslContext.select()).thenReturn(mockSelectStep)
    }


    @Test
    fun `test getBrands`() {
        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        `when`(mockSelectStep.fetchInto(BrandRecord::class.java)).thenReturn(listOf<BrandRecord>())
        `when`(dslContext.select().from(BRAND)).thenReturn(mockSelectStep)

        val result = brandRepository.getBrands()

    }

    @Test
    fun `test getBrandById`() {
        val brandId = 1L

        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        val mockSelectConditionStep: SelectConditionStep<Record> =
            mock(SelectConditionStep::class.java) as SelectConditionStep<Record>
        val mockBrandRecord = mock(BrandRecord::class.java)

        `when`(mockSelectConditionStep.fetchOneInto(BrandRecord::class.java)).thenReturn(mockBrandRecord)
        `when`(dslContext.select().from(BRAND)).thenReturn(mockSelectStep)
        `when`(mockSelectStep.where(BRAND.BRAND_ID.eq(brandId))).thenReturn(mockSelectConditionStep)

        val result = brandRepository.getBrandById(brandId)
        assertEquals(mockBrandRecord, result)
    }

    @Test
    fun `test getBrandByName`() {
        val brandName = "Test Brand"

        val mockSelectStep: SelectJoinStep<Record> = mock(SelectJoinStep::class.java) as SelectJoinStep<Record>
        val mockSelectConditionStep: SelectConditionStep<Record> = mock(SelectConditionStep::class.java) as SelectConditionStep<Record>
        val mockBrandRecord = mock(BrandRecord::class.java)

        `when`(mockSelectConditionStep.fetchOneInto(BrandRecord::class.java)).thenReturn(mockBrandRecord)
        `when`(dslContext.select().from(BRAND)).thenReturn(mockSelectStep)
        `when`(mockSelectStep.where(BRAND.BRAND_NAME.eq(brandName))).thenReturn(mockSelectConditionStep)

        val result = brandRepository.getBrandByName(brandName)
        assertEquals(mockBrandRecord, result)
    }

}
