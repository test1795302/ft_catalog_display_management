package com.springdemo.ft_catalog_display.graphql

import com.springdemo.ft_catalog_display.dto.CategoryDto
import com.springdemo.ft_catalog_display.service.CategoryService
import graphql.kickstart.tools.GraphQLMutationResolver
import graphql.kickstart.tools.GraphQLQueryResolver
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Component


@Component
class CategoryResolver (private val categoryService: CategoryService) : GraphQLQueryResolver, GraphQLMutationResolver {
    private val logger : Logger = LoggerFactory.getLogger(CategoryResolver::class.java)
    fun getAllCategories(): List<CategoryDto>  {
        logger.info("Entered getAllCategories with 0 arguments")
        val listOfCategories = categoryService.getCategories()
        logger.info("Returning request with $listOfCategories")
        return listOfCategories
    }
}
