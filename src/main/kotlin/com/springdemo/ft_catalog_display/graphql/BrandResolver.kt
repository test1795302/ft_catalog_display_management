package com.springdemo.ft_catalog_display.graphql

import com.springdemo.ft_catalog_display.dto.BrandDto
import com.springdemo.ft_catalog_display.service.BrandService
import graphql.kickstart.tools.GraphQLMutationResolver
import graphql.kickstart.tools.GraphQLQueryResolver
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class BrandResolver(private val brandService: BrandService) : GraphQLQueryResolver, GraphQLMutationResolver {
    private val logger : Logger = LoggerFactory.getLogger(BrandResolver::class.java)
    fun getAllBrands(): List<BrandDto> {
        logger.info("Entered getAllBrands with 0 arguments")
        val listOfBrands = brandService.getBrands()
        logger.info("Returning request with $listOfBrands")
        return listOfBrands
    }
}
