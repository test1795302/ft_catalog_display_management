package com.springdemo.ft_catalog_display.graphql

import com.springdemo.ft_catalog_display.dto.ProductDto
import com.springdemo.ft_catalog_display.service.ProductService
import graphql.kickstart.tools.GraphQLMutationResolver
import graphql.kickstart.tools.GraphQLQueryResolver
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class ProductResolver (private val productService: ProductService) : GraphQLQueryResolver, GraphQLMutationResolver {

    private val logger : Logger = LoggerFactory.getLogger(ProductResolver::class.java)
    fun getProductById(productId:Long): ProductDto? {
        logger.info("Entered getProductById with productId : $productId")
        val product = productService.getProductById(productId)
        if (product == null) logger.warn("product with id $productId not found")
        else    logger.info("Returning request for id: $productId with $product")
        return product
    }
    fun getAllProducts():List<ProductDto> {
        logger.info("Entered getAllProducts with 0 arguments")
        val listOfProducts = productService.getAllProducts()
        logger.info("Returning request with $listOfProducts")
        return listOfProducts
    }

    fun filterProducts(country: String?,category: String?,feature: List<String>?,brand: List<String>?, searchField: String?, sortField: String?, pageNumber:Long,limit: Long):List<ProductDto> {
        logger.info("Entering filterProduct with filters country: $country, category: $category, feature: $feature, Brand: $brand, pageNumber: $pageNumber, limit: $limit}")
        val listOfProducts = productService.filterProducts(country,category,feature,brand, searchField, sortField, pageNumber,limit)
        logger.info("Returning request with $listOfProducts")
        return listOfProducts
    }
}
