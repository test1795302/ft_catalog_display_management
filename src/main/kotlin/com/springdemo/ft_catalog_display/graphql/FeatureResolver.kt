package com.springdemo.ft_catalog_display.graphql

import com.springdemo.ft_catalog_display.dto.FeatureDto
import com.springdemo.ft_catalog_display.service.FeatureService
import graphql.kickstart.tools.GraphQLMutationResolver
import graphql.kickstart.tools.GraphQLQueryResolver
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class FeatureResolver  (private val featureService: FeatureService) : GraphQLQueryResolver, GraphQLMutationResolver {

    private val logger : Logger = LoggerFactory.getLogger(FeatureResolver::class.java)
    fun getAllFeatures() : List<FeatureDto> {
        logger.info("Entered getAllFeatures with 0 arguments")
        val listOfFeatures = featureService.getAllFeatures()
        logger.info("Returning request with $listOfFeatures")
        return listOfFeatures
    }
}
