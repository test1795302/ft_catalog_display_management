package com.springdemo.ft_catalog_display.graphql

import com.springdemo.ft_catalog_display.dto.CountryDto
import com.springdemo.ft_catalog_display.service.CountryService
import graphql.kickstart.tools.GraphQLMutationResolver
import graphql.kickstart.tools.GraphQLQueryResolver
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import kotlin.math.log

@Component
class CountryResolver  (private val countryService: CountryService) : GraphQLQueryResolver, GraphQLMutationResolver {
    private val logger : Logger = LoggerFactory.getLogger(CountryResolver::class.java)
    fun getAllCountries(): List<CountryDto> {
        logger.info("Entered getAllCountries with 0 arguments")
        val listOfCountries = countryService.getAllCountries()
        logger.info("Returning request with $listOfCountries")
        return listOfCountries
    }
}
