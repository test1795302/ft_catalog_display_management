package com.springdemo.ft_catalog_display.repository

import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.Tables.COUNTRY
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.CountryRecord
import org.jooq.DSLContext
import org.springframework.stereotype.Repository

@Repository
class CountryRepository(private val dslContext: DSLContext) {

    fun getAllCountries() : List<CountryRecord> {
        return dslContext.select()
            .from(COUNTRY)
            .fetchInto(CountryRecord::class.java)
    }

    fun getCountryById(countryId: Long) : CountryRecord {
        val countryRecord: CountryRecord? = dslContext
            .select()
            .from(COUNTRY)
            .where(COUNTRY.COUNTRY_ID.eq(countryId))
            .fetchOneInto(CountryRecord::class.java)
        return countryRecord ?: throw NoSuchElementException("No country found with id: $countryId")
    }

    fun getCountryByName(countryName: String) : CountryRecord? {
        return dslContext
            .select()
            .from(COUNTRY)
            .where(COUNTRY.COUNTRY_NAME.eq(countryName))
            .fetchOneInto(CountryRecord::class.java)
    }
}
