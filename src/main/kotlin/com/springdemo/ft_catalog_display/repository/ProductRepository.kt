package com.springdemo.ft_catalog_display.repository

import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.Brand.BRAND
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.Country.COUNTRY
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.Product.PRODUCT
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.ProductFeature.PRODUCT_FEATURE
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.Category.CATEGORY
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.Feature.FEATURE
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.ProductRecord
import org.jooq.DSLContext
import org.jooq.Record
import org.jooq.SelectJoinStep
import org.springframework.stereotype.Repository
import java.math.BigDecimal


@Repository
class ProductRepository(private val dslContext: DSLContext) {
    fun getProducts() : List<ProductRecord> {
        return dslContext.select()
            .from(PRODUCT)
            .fetchInto(ProductRecord::class.java)
    }

    fun getProductById(productId: Long) : ProductRecord {

        val productRecord : ProductRecord? = dslContext.select()
            .from(PRODUCT)
            .where(PRODUCT.PRODUCT_ID.eq(productId))
            .fetchOneInto(ProductRecord::class.java)
        return productRecord ?: throw NoSuchElementException("ProductRecord not found with productId $productId")
    }

    fun filterProducts(country: String?,category: String?,feature: List<String>?,brand: List<String>?, search: String?, sort: String?, pageNumber:Long,limit: Long): List<ProductRecord> {
        val select: SelectJoinStep<Record> = dslContext.select().from(PRODUCT)

        if (!country.isNullOrEmpty()) {
            select.join(COUNTRY)
                .on(PRODUCT.COUNTRY_ID.eq(COUNTRY.COUNTRY_ID))
                .where(COUNTRY.COUNTRY_NAME.eq(country))
        }

        if (!category.isNullOrEmpty()) {
            select.join(CATEGORY)
                .on(PRODUCT.CATEGORY_ID.eq(CATEGORY.CATEGORY_ID))
                .where(CATEGORY.CATEGORY_NAME.eq(category))
        }

        if (!brand.isNullOrEmpty()) {
            select.join(BRAND)
                .on(PRODUCT.BRAND_ID.eq(BRAND.BRAND_ID))
                .where(BRAND.BRAND_NAME.`in`(brand))
        }

        if (!feature.isNullOrEmpty()) {


            select.join(PRODUCT_FEATURE)
                .on(PRODUCT.PRODUCT_ID.eq(PRODUCT_FEATURE.PRODUCT_ID))
                .where(PRODUCT_FEATURE.FEATURE_ID.`in`(dslContext.select(FEATURE.FEATURE_ID).from(FEATURE).where(FEATURE.FEATURE_NAME.`in`(feature))))
        }

        if (!search.isNullOrEmpty()) {
            select.where(PRODUCT.TITLE.likeIgnoreCase("%${search}%"))
        }

        select.limit(limit)

        select.offset(limit * (pageNumber - 1))

        select.orderBy(
            if (!sort.isNullOrEmpty())
                if (sort == "ASC") PRODUCT.PRICE.asc() else PRODUCT.PRICE.desc()
            else
                PRODUCT.PRODUCT_ID.asc()
        )

        return select.fetchInto(ProductRecord::class.java)
    }
}