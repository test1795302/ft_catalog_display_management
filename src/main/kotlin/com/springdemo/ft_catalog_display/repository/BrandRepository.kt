package com.springdemo.ft_catalog_display.repository

import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.Brand
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.BrandRecord
import org.jooq.DSLContext
import org.springframework.stereotype.Repository
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.Brand.BRAND
@Repository
class BrandRepository(private val dslContext: DSLContext) {

    fun getBrands() : List<BrandRecord> {
        return dslContext.select()
            .from(BRAND)
            .fetchInto(BrandRecord::class.java)
    }

    fun getBrandById(brandId : Long) : BrandRecord {
        val brandRecord : BrandRecord? = dslContext
            .select()
            .from(BRAND)
            .where(BRAND.BRAND_ID.eq(brandId))
            .fetchOneInto(BrandRecord::class.java)
        return brandRecord ?: throw NoSuchElementException("No brand found with id $brandId")
    }

    fun getBrandByName(brandName: String) : BrandRecord? {
        return dslContext
            .select()
            .from(BRAND)
            .where(BRAND.BRAND_NAME.eq(brandName))
            .fetchOneInto(BrandRecord::class.java)
    }
}