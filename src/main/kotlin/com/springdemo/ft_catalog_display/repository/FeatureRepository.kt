package com.springdemo.ft_catalog_display.repository

import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.FeatureRecord
import org.jooq.DSLContext
import org.springframework.stereotype.Repository
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.Feature.FEATURE
@Repository
class FeatureRepository(private val dslContext: DSLContext) {

    fun getAllFeatures() : List<FeatureRecord> {
        return dslContext.select()
            .from(FEATURE)
            .fetchInto(FeatureRecord::class.java)
    }

    fun getFeatureByName(featureName: String) : FeatureRecord {
        val featureRecord: FeatureRecord? = dslContext.select()
            .from(FEATURE)
            .where(FEATURE.FEATURE_NAME.eq(featureName))
            .fetchOneInto(FeatureRecord::class.java)
        return featureRecord ?: throw NoSuchElementException("Feature not found with name $featureName")
    }

    fun getFeatureById(featureId: Long) : FeatureRecord {
        val featureRecord: FeatureRecord? = dslContext
            .select()
            .from(FEATURE)
            .where(FEATURE.FEATURE_ID.eq(featureId))
            .fetchOneInto(FeatureRecord::class.java)
        return featureRecord ?: throw NoSuchElementException("No feature found with id: $featureId")
    }
}