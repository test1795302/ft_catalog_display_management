package com.springdemo.ft_catalog_display.repository

import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.CategoryRecord
import org.jooq.DSLContext
import org.springframework.stereotype.Repository
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.Category.CATEGORY
@Repository
class CategoryRepository(private val dslContext: DSLContext) {

    fun getCategories() : List<CategoryRecord> {
        return dslContext.select()
            .from(CATEGORY)
            .fetchInto(CategoryRecord::class.java)
    }

    fun getCategoryById(categoryId: Long) : CategoryRecord {
        val categoryRecord : CategoryRecord? = dslContext
            .select()
            .from(CATEGORY)
            .where(CATEGORY.CATEGORY_ID.eq(categoryId))
            .fetchOneInto(CategoryRecord::class.java)
        return categoryRecord ?: throw NoSuchElementException("Category Not found with category Id: $categoryId")
    }

    fun getCategoryByName(categoryName: String) : CategoryRecord? {
        return dslContext
            .select()
            .from(CATEGORY)
            .where(CATEGORY.CATEGORY_NAME.eq(categoryName))
            .fetchOneInto(CategoryRecord::class.java)
    }
}