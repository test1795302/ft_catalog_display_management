package com.springdemo.ft_catalog_display.Controller

import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.nio.file.Path
import java.nio.file.Paths

@RestController
class ImageController {

    private val imageDirectory: Path = Paths.get("images")

    @GetMapping("/{imageName}")
    @CrossOrigin
    fun sendImage(@PathVariable imageName: String) : ResponseEntity<Resource> {
        try {
            val imagePath: Path = imageDirectory.resolve(imageName)
            val resource: Resource = UrlResource(imagePath.toUri())

            return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=${resource.filename}")
                .contentType(MediaType.IMAGE_PNG)
                .body(resource)
        }
        catch (e: Exception) {
            return ResponseEntity.notFound().build()
        }
    }
}