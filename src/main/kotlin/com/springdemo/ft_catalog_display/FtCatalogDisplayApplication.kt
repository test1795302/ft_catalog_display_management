package com.springdemo.ft_catalog_display

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@SpringBootApplication
@CrossOrigin
class FtCatalogDisplayApplication

fun main(args: Array<String>) {
	runApplication<FtCatalogDisplayApplication>(*args)
}

@Bean
fun corsFilter(): CorsFilter {
	val source = UrlBasedCorsConfigurationSource()
	val config = CorsConfiguration()
	config.addAllowedOrigin("*")  // You can specify allowed origins here
	config.addAllowedHeader("*")
	config.addAllowedMethod("*")
	source.registerCorsConfiguration("/**", config)
	return CorsFilter(source)
}

@Bean
fun corsConfigurer(): WebMvcConfigurer {
	return object : WebMvcConfigurer {
		override fun addCorsMappings(registry: CorsRegistry) {
			registry.addMapping("/**").allowedOrigins("*")
		}
	}
}








