package com.springdemo.ft_catalog_display

import com.springdemo.ft_catalog_display.graphql.*
import graphql.ExecutionResult
import graphql.GraphQL
import graphql.kickstart.tools.SchemaParser
import graphql.schema.GraphQLSchema
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.core.io.Resource
import org.springframework.context.annotation.ComponentScan
import org.springframework.web.bind.annotation.CrossOrigin

@ComponentScan(basePackages = ["com.springdemo.ft_catalog_display.graphql"])
@RestController
@RequestMapping("/graphql")
@CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
class GraphQLController(private val categoryResolver: CategoryResolver,private val featureResolver: FeatureResolver,private val brandResolver: BrandResolver,private val productResolver: ProductResolver){
    @Value("classpath:schema.graphqls")
    private lateinit var schemaFilePath: Resource

    @PostMapping
    @CrossOrigin
    fun executeQuery(@RequestBody query: String): ResponseEntity<Any> {
        val graphQL: GraphQL = createGraphQLInstance()
        val executionResult: ExecutionResult = graphQL.execute(query)
        return ResponseEntity.ok(executionResult.getData())
    }

    private fun createGraphQLInstance(): GraphQL {
        val schema: GraphQLSchema = SchemaParser.newParser()
            .file("classpath:schema.graphqls")
            .resolvers(categoryResolver,featureResolver,brandResolver,productResolver)
            .build()
            .makeExecutableSchema()

        return GraphQL.newGraphQL(schema).build()
    }

}



