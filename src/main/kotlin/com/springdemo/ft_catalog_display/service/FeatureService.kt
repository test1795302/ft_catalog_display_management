package com.springdemo.ft_catalog_display.service

import com.springdemo.ft_catalog_display.dto.FeatureDto
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.FeatureRecord
import com.springdemo.ft_catalog_display.repository.FeatureRepository
import org.springframework.stereotype.Service

@Service
class FeatureService(private val featureRepository: FeatureRepository) {

    fun getAllFeatures() : List<FeatureDto> {
        return featureRepository.getAllFeatures().map { featureRecordToFeatureDto(it) }
    }

    private fun featureRecordToFeatureDto(featureRecord: FeatureRecord) : FeatureDto {
        return FeatureDto(
            featureId = featureRecord.featureId,
            featureName = featureRecord.featureName
        )
    }
}