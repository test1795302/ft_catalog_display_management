package com.springdemo.ft_catalog_display.service

import com.springdemo.ft_catalog_display.dto.CountryDto
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.CountryRecord
import com.springdemo.ft_catalog_display.repository.CountryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CountryService(@Autowired private val countryRepository: CountryRepository) {

    fun getAllCountries() : List<CountryDto> {
        return countryRepository.getAllCountries().map { countryRecordToCountryDto(it) }
    }

    fun getCountryById(countryId: Long) : CountryDto {
        return countryRecordToCountryDto(countryRepository.getCountryById(countryId))
    }
    private fun countryRecordToCountryDto(countryRecord: CountryRecord) : CountryDto {
        return CountryDto(
            countryId = countryRecord.countryId,
            countryName = countryRecord.countryName
        )
    }
}
