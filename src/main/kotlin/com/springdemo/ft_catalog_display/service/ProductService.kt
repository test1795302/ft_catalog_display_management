package com.springdemo.ft_catalog_display.service

import com.springdemo.ft_catalog_display.dto.ProductDto
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.ProductRecord
import com.springdemo.ft_catalog_display.repository.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ProductService (@Autowired private val productRepository: ProductRepository,
    @Autowired private val countryService: CountryService,@Autowired private val categoryService: CategoryService,@Autowired private val brandService: BrandService){

    fun getProductById(productId: Long) : ProductDto? {
        return productRepository.getProductById(productId)?.let { productRecordToProductDto(it) }
    }

    fun getAllProducts() : List<ProductDto> {
        return productRepository.getProducts().map { productRecordToProductDto(it) }
    }

    fun filterProducts(country: String?,category: String?,feature: List<String>?,brand: List<String>?, searchField: String?, sortField: String?,pageNumber:Long,limit: Long):List<ProductDto>{
        return productRepository.filterProducts(country,category,feature,brand, searchField, sortField, pageNumber,limit).map{ productRecordToProductDto(it) }
    }

    private fun productRecordToProductDto(productRecord: ProductRecord) : ProductDto {
        return ProductDto(
            productId = productRecord.productId,
            price = productRecord.price,
            title = productRecord.title,
            description = productRecord.description,
            logo = productRecord.logo,
            country = countryService.getCountryById(productRecord.countryId),
            brand = brandService.getBrandsById(productRecord.brandId),
            category = categoryService.getCategory(productRecord.categoryId),
        )
    }
}