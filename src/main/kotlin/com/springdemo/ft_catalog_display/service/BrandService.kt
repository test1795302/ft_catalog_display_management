package com.springdemo.ft_catalog_display.service

import com.springdemo.ft_catalog_display.dto.BrandDto
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.BrandRecord
import com.springdemo.ft_catalog_display.repository.BrandRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BrandService(@Autowired private val brandRepository: BrandRepository) {

    fun getBrands() : List<BrandDto> {
        return brandRepository.getBrands().map { brandRecordToBrandDto(it) }
    }

    fun getBrandsById(brandId: Long) : BrandDto {
        return brandRecordToBrandDto(brandRepository.getBrandById(brandId))
    }

    private fun brandRecordToBrandDto(brandRecord: BrandRecord) : BrandDto {
        return BrandDto(
            brandId = brandRecord.brandId,
            brandName = brandRecord.brandName
        )
    }
}