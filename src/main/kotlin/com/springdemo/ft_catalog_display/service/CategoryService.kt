package com.springdemo.ft_catalog_display.service

import com.springdemo.ft_catalog_display.dto.CategoryDto
import com.springdemo.ft_catalog_display.jooq.ft_catalog_display.tables.records.CategoryRecord
import com.springdemo.ft_catalog_display.repository.CategoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CategoryService(@Autowired private val categoryRepository: CategoryRepository) {

    fun getCategories() : List<CategoryDto> {
        return categoryRepository.getCategories().map { categoryRecordToCategoryDto(it) }
    }

    fun getCategory(categoryId: Long) : CategoryDto {

        return categoryRecordToCategoryDto(categoryRepository.getCategoryById(categoryId))
    }

    private fun categoryRecordToCategoryDto(categoryRecord: CategoryRecord) : CategoryDto {
        return CategoryDto(
            categoryId = categoryRecord.categoryId,
            categoryName = categoryRecord.categoryName
        )
    }
}