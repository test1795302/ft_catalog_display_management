package com.springdemo.ft_catalog_display.dto

data class CategoryDto (
    val categoryId : Long?,
    val categoryName : String,
)
