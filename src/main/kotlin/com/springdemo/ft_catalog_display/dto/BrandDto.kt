package com.springdemo.ft_catalog_display.dto


data class BrandDto (
    val brandId : Long?,
    val brandName : String,

)
