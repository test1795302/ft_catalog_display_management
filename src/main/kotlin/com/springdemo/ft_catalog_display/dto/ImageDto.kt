package com.springdemo.ft_catalog_display.dto

data class ImageDto (
    val imageId : Long?,
    val uri : String,
)