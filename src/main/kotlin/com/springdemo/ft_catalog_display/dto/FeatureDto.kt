package com.springdemo.ft_catalog_display.dto

data class FeatureDto (
    val featureId : Long?,
    val featureName : String,
)
