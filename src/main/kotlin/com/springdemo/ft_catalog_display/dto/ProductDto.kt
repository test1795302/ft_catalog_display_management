package com.springdemo.ft_catalog_display.dto


data class ProductDto(
    val productId: Long?,
    val title: String,
    val description: String,
    val price: Double,
    val logo: String,
    val country: CountryDto,
    val category: CategoryDto?,
    val brand : BrandDto?,
)