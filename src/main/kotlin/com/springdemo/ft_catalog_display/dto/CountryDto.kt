package com.springdemo.ft_catalog_display.dto
data class CountryDto(
    val countryId : Long?,
    val countryName : String,
)
